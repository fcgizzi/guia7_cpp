# Métodos de búsqueda

Se solicita un programa que cree un arreglo mediante funciones de tabla hash, este debe almacenar números en el arreglo y también debe permitir buscarlos dentro de este, si existen colisiones, se deben solucionar mediante funciones de metodos (prueba lineal, prueba cuadratica, doble hash y encadenamiento), el cual debe ser elegido por el usuario.

# Compilación
- Se escribe `make` en la terminal, si no se tiene, se instala con anterioridad ingresando `sudo apt install make` en la terminal

- Si no se tiene make, se usa `g++ main.cpp Arreglo.cpp Lista.cpp -o main` para compilar

# Ejecución

Para ejecutar el programa el programa se ingresa `./main` en la terminal, además de esto se agrega uno de los métodos con los que quiere trabajar el usuario (L, C, D y E).

# Programa

Al ingresar con uno de los métodos, el programa solicitará ingresar el tamañoo del arreglo que desee utilizar el usuario.
Luego de esto saldrá un menú con tres opciones:
    1- Ingresar número: opción para ingresar números en el arreglo, el cual posteriormente imprimirá antes de volver al menú principal
    2- Buscar número: opción para busqueda de número, el cual bscará el número deseado en el arreglo e imprimirá su posición.
    otro- salir: opción para salir.

# Requisitos

- Sistema operativo Linux
- Make instalado 

# Construido con
- Linux
- Geany
- Lenguaje c++

# Autor
- Franco Cifuentes Gizzi
