// librerias
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
using namespace std;

// clases
#include "main.h"
#include "Lista.h"

// se define la clase
Lista::Lista(){}

// función agrega número en la lista
void Lista::agregar_numero(int numero){
	Nodo *tmp;
    // variables crea nodo temporal
    // y asigna un número
    // sig queda null por defecto
    tmp = new Nodo;
    tmp-> numero = numero;
    tmp->sig = NULL;

    // primer nodo queda como raiz y último
    if(this-> raiz == NULL){
        this-> raiz = tmp;
        this-> ultimo = this-> raiz;
    }
    //sino apunta al último nodo y deja el nuevo como último
    else{
		this-> ultimo-> sig = tmp;
		this-> ultimo = tmp;
    }
}

// función imprime la lista
void Lista::imprimir(){
	// recorre la lista
	Nodo *tmp = this-> raiz;
	if(tmp==NULL){
		cout << "\n\t >> No se encuentran colisiones" << endl;
	}
	// recorre hasta que llege al final
	cout << "\n\t Colisiones" << endl;
	while(tmp != NULL){
		cout << "| " << tmp-> numero << " |";
		tmp = tmp->sig;
	}
	cout << "\n";
}
