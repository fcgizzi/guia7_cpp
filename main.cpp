/*
 * Compilación: $ make
 * Ejecución: $ ./main junto con uno de los métodos (L, C, D o E))
 */
 
// librerias
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
using namespace std;

// clases
#include "main.h"
#include "Lista.h"
#include "Arreglo.h"

// función ingresa número normal o por colisión
void ingresar(Arreglo f_arreglo, int n, int *arreglo, string metodo, Lista *lista){
	// numero a agregar
	int num;
	system("clear");
	cout << "\n\t >> Número que desee agregar: ";
	cin >> num;
	cout << "\n";

	// se encuentra posición por función hash
	int posicion = f_arreglo.funcion_hash(num, n);

	// si no hay colisión se agrega de manera normal
	if(arreglo[posicion]==NULL){
		arreglo[posicion] = num;
	}
	// en cambio, si hay colisión se agrega según método
	else{
		// prueba linea
		if(metodo == "L" || metodo == "l"){
			f_arreglo.prueba_lineal(n, arreglo, num, posicion, false);
		}
		// prueba cuadrática
		else if(metodo == "C" || metodo == "c"){
			f_arreglo.prueba_cuadratica(n, arreglo, num, posicion, false);
		}
		// doble dirección
		else if(metodo == "D" || metodo == "d"){
			f_arreglo.doble_direccion(n, arreglo, num, posicion, false);
		}
		// encadenamiento
		else if(metodo == "E" || metodo == "e"){
			f_arreglo.encadenamiento(arreglo, n, num, posicion, lista, false);
		}
	}
}

// función que busca el número que se desee de forma normal o por colisión
void buscar(Arreglo f_arreglo, int n, int *arreglo, string metodo, Lista *lista){
	int num;
	system("clear");
	cout << "\n\t >> Número para buscar: ";
	cin >> num;

	// se encuentra posición por función hash
	int posicion = f_arreglo.funcion_hash(num, n);

	// se señala si lo encuentra
	if(arreglo[posicion] == num){
		cout << "\t >> El número " << num << " existe en la posicion " << posicion + 1 << endl;
	}
	// si no lo encuentra se le dice al usuario
	else if(arreglo[posicion]!= num && arreglo[posicion] == NULL){
		cout << "\t >> El número " << num << " no existe en el arreglo" << endl;
	}
	else if(arreglo[posicion]!=num && arreglo[posicion]!=NULL){
		if(metodo == "L" || metodo == "l"){
			f_arreglo.prueba_lineal(n, arreglo, num, posicion, true);
		}
		else if(metodo == "C" || metodo == "c"){
			f_arreglo.prueba_cuadratica(n, arreglo, num, posicion, true);
		}
		else if(metodo == "D" || metodo == "d"){
			f_arreglo.doble_direccion(n, arreglo, num, posicion, true);
		}
		else if(metodo == "E" || metodo == "e"){
			f_arreglo.encadenamiento(arreglo, n, num, posicion, lista, true);
		}
	}
}

// función menú de opciones del programa
void menu(int n, int *arreglo, string metodo, Lista *lista){
	// se definen las variables
	Arreglo f_arreglo;
	int op;

	bool llena = f_arreglo.is_full(n, arreglo);
	// si el arreglo está lleno se detiene
	if(llena == false){
		// usuario ingresa opción
		cout << "\n\t------------Menú-------------" << endl;
		cout << "\t| Elija una opción:         |" << endl;
		cout << "\t| 1 >> Insertar número      |" << endl;
		cout << "\t| 2 >> Buscar número        |" << endl;
		cout << "\t| otro >> Salir             |" << endl;
		cout << "\t-----------------------------" << endl;
		cout << "\t >> ";
		cin >> op;

		if(op == 1){
			// se llama a función para ingreso de numero
			ingresar(f_arreglo, n, arreglo, metodo, lista);
			// imprimir arreglo
			cout << "\t Lista";
			cout << "\t";
			for(int i=0; i<n; i++){
				if(arreglo[i]==NULL){
					cout << "[ ]";
				}
				else{
					cout << "[" << arreglo[i] << "]";
				}
			}
			cout << "\n";
			// se vuelve al menu
			menu(n, arreglo, metodo, lista);
		}
	
		if(op == 2){
			//busca el numero y luego se devuelve al menu
			buscar(f_arreglo, n, arreglo, metodo, lista);
			cout << "\t Lista";
			cout << "\t";
			for(int i=0; i<n; i++){
				if(arreglo[i]==NULL){
					cout << "[ ]";
				}
				else{
					cout << "[" << arreglo[i] << "]";
				}
			}
			cout << "\n";
			menu(n, arreglo, metodo, lista);
		}
		else{
			system("clear");
			cout << "\t --ADIOS--" << endl;
		}
	}
		
}

// función deja la lista de manera vacía, o sea llena de null
void lista_null(int n, int *arreglo){
	for(int i=0; i<n; i++){
		arreglo[i] = NULL;
	}
}

// función de menú
int main(int argc, char *argv[]){
	// se detetmina el método de solución de colisión
	string metodo = argv[1];
	
	// se verifica 	que sea valido lo ingresado
	bool valido;
	// tiene que validarse con mayusculas y minusculas
	if(metodo == "L" or metodo == "l"){
		valido = true;
	}	
	else if(metodo == "C" or metodo == "c"){
		valido = true;
	}
	else if(metodo == "D" or metodo == "d"){
		valido = true;
	}
	else if(metodo == "E" or metodo == "e"){
		valido = true;
	}
	else{
		valido = false;
	}
	
	if(valido){
		// definicion e ingreso del tamaño del arreglo
		int n;
		cout << "\n\t >> Tamaño para el arreglo: ";
		cin >> n;
		system("clear");

		// el arreglo tiene que se mayor a 0
		if(n>0){
			int arreglo[n];
			// llena la lista con null
			lista_null(n, arreglo);
			
			// se llama a la lista enlazada
			Lista *lista = new Lista();
			system("clear");
			// se llama al menú de opciones
			menu(n, arreglo, metodo, lista);
		}
		else{
			main(argc, argv);
		}
	}
  return 0;
}
