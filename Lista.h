// libreria
#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

// se define clase
class Lista {
    private:
        Nodo *raiz = NULL;
        Nodo *ultimo = NULL;
	// funciones
    public:
        Lista();
        // función agrega número y crea nuevo nodo
        void agregar_numero(int numero);
        // función muestra la lista
        void imprimir();
};
#endif
