// libreria
#include <iostream>
using namespace std;

#ifndef ARREGLO_H
#define ARREGLO_H

// se define la clase
class Arreglo{
	public:
		// constructor
		Arreglo();

		// funciones y métodos
		void prueba_lineal(int n, int *arreglo, int num, int posicion, bool busqueda);
		void prueba_cuadratica(int n, int *arreglo, int num, int posicion, bool busqueda);
		int primo(int n);
		int funcion_hash(int numero, int n);
		void doble_direccion(int n, int *arreglo, int num, int posicion, bool busqueda);
		void encadenamiento(int *arreglo, int n, int num, int posicion, Lista *lista, bool busqueda);
		bool is_full(int n, int *arreglo);
	private:
};
#endif
