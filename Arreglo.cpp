// libreria
#include <iostream>
using namespace std;

#include "main.h"
#include "Lista.h"
#include "Arreglo.h"

// constructor
Arreglo::Arreglo(){}

// función método de colisiones pueba lineas
void Arreglo::prueba_lineal(int n, int *arreglo, int num, int posicion, bool busqueda){
	// se busca existencia del elemento en la posición
	if(arreglo[posicion] != NULL and arreglo[posicion] == num){
		cout << "\t >> EL elemento ya existe en la posición " << posicion + 1 << endl;
	}
	// si no está se agrega o busca
	else{
		// se crea posición temporal
		int posicion_temp = posicion + 1;
		// mientras la posicion sea menor al largo y no sea igual al inicio
		while(posicion_temp <= n and arreglo[posicion_temp] != NULL and arreglo[posicion] != num and posicion_temp != posicion){
			// si se busca y encuentra, detiene el ciclo
			if(busqueda){
				if(arreglo[posicion_temp] == num){
					break;
				}
			}
			// se aumenta para seguir buscando, al llegar al límite se vuelve al inicio
			posicion_temp++;
			if(posicion_temp==n){
				posicion_temp = 0;
			}
		}
		// se agrega si no se está buscando
		if(busqueda == false){
			arreglo[posicion_temp] = num;
		}
		// no se encuentra en el arreglo
		if(arreglo[posicion_temp] == NULL or posicion_temp == posicion){
			cout << "\n\t >> No se ha encontrado" << endl;
		}
		else{
			cout << "\n\t >> El número se ha encontrado en la posición: " << posicion_temp + 1 << endl;
		}
		cout << "\t >> Colisión en posición: " << posicion << endl;
		cout << "\t >> El número fue encontrado en la posición: " << posicion_temp + 1 << endl;
		
		// desplazamiento final del número
		int d_final = posicion_temp - posicion;
		if(d_final<0){
			d_final = posicion - posicion_temp;
		}
		cout << "\t >> Desplazamiento final: " << d_final << endl;
	}
}

// función método de colisiones prueba cuadrática
void Arreglo::prueba_cuadratica(int n, int *arreglo, int num, int posicion, bool busqueda){
	// número existe
	if(arreglo[posicion] != NULL and arreglo[posicion] == num){
		cout << "\t >> EL elemento ya existe en la posición " << posicion + 1 << endl;
	}
	// se busca o agrega
	else{
		int i = 1;
		int posicion_temp = posicion + (i*i);
		
		// si no está vacío y no es donde se quiere agregar, si se encuentra el ciclo se detiene
		while(arreglo[posicion_temp] != NULL and arreglo[posicion_temp] != num){
			if(busqueda){
				if(arreglo[posicion_temp] == num){
					break;
				}
			}
			// sigue buscando
			i++;
			posicion_temp = posicion + (i*i);
			// si excede el límite vuelve al inicio
			if(posicion_temp >= n){
				i = 0;
				posicion_temp = 1;
				posicion = 1;
			}
		}
		// se agrea si es que no busca
		if(busqueda == false){
			arreglo[posicion_temp] = num;
		}
		// imprime lo encontrado
		if(arreglo[posicion_temp] == NULL){
			cout << "\n\t >> No se ha encontrado" << endl;
		}
		else{
			cout << "\n\t >> El número se ha encontrado en la posición: " << posicion_temp + 1 << endl;
		}
		cout << "\t >> Colisión en posición: " << posicion << endl;
		cout << "\t >> El número fue encontrado en la posición: " << posicion_temp + 1 << endl;
		// desplazamiento final
		int d_final = posicion_temp - posicion;
		if(d_final<0){
			d_final = posicion - posicion_temp;
		}
		cout << "\t >> Desplazamiento final: " << d_final << endl;
	}
}

// función que determina el número primo antecesor
int Arreglo::primo(int n){
	n = n+1;
	// contador de divisores
	int cont;
	do{
		// se inicia el contador de 0
		// se elimina un n
		cont = 0;
		n--;
		// for para determinar primos
		for(int i = n; i >= 1; i--){
			if((n%i)==0){
				cont++;
			}
		}
	}
	// para que no sea primo el contador debe ser mayor que 2
	while(cont>2 and n>0);
		return n;
}

// funcion hash por módulo
int Arreglo::funcion_hash(int num, int n){
	// debe ser número primo
	int divisor = primo(n);
	return (num%divisor);
}

// función método de colisiones doble dirección
void Arreglo::doble_direccion(int n, int *arreglo, int num, int posicion, bool busqueda){
	// si ya existe
	if(arreglo[posicion]!=NULL and arreglo[posicion]==num){
		cout << "\t >> EL elemento ya existe en la posición " << posicion + 1 << endl;
	}
	//sino se busca
	else{
		// se usa función para la posición
		int posicion_temp = funcion_hash(posicion+1, n);
		while(posicion_temp <= n and arreglo[posicion_temp] != NULL and arreglo[posicion_temp] != num and posicion_temp != posicion){
			// si encuentra lo que se busca el ciclo termina
			if(busqueda){
				if(arreglo[posicion_temp]==num){
					break;
				}
			}
			// se sigue buscando
			posicion_temp = funcion_hash(posicion_temp+1, n);
		}
		// se agrega al arreglo
		if(busqueda == false){
			arreglo[posicion_temp] = num;
		}
		// imprime posición
		if(arreglo[posicion_temp]==NULL or arreglo[posicion_temp]!=num){
			cout << "\n\t >> No se ha encontrado" << endl;
		}
		else{
			cout << "\n\t >> El número se ha encontrado en la posición: " << posicion_temp + 1 << endl;
		}
		cout << "\t >> Colisión en posición: " << posicion << endl;
		cout << "\t >> El número fue encontrado en la posición: " << posicion_temp + 1 << endl;
		int d_final = posicion_temp - posicion;
		if(d_final<0){
			d_final = posicion - posicion_temp;
		}
		cout << "\t >> Desplazamiento final: " << d_final << endl;
	}
}

// función método de colisiones por encadenamiento
void Arreglo::encadenamiento(int *arreglo, int n, int num, int posicion, Lista *lista, bool busqueda){
	cout << "\t >> Colisión en posición: " << posicion + 1 << endl;
	// nodos
	Nodo *tmp = NULL;
	Nodo *arreglo_temp[n];
	// arreglo tipo nodo
	for(int i = 0; i < n; i++){
		arreglo_temp[i] = new Nodo();
		arreglo_temp[i]-> sig = NULL;
		arreglo_temp[i]-> numero = NULL;
	}
	// se llena
	for(int i=0; i<n; i++){
		arreglo_temp[i]-> numero = arreglo[i];
		arreglo_temp[i]-> sig = NULL;
	}
	
	// si ya existe
	if(arreglo[posicion]!=NULL and arreglo[posicion]==num){
		cout << "\t >> EL elemento ya existe en la posición " << posicion + 1 << endl;
	}
	// sino se busca
	else{
		// temporal
		tmp = arreglo_temp[posicion]->sig;
		// ciclo while mientra no sea vacío y el número sea diferente al que se está buscando
		while(tmp != NULL and tmp -> numero != num){
			tmp = tmp-> sig;
		}
		if(busqueda==false){
			lista->agregar_numero(num);
			cout << "\t El número fue agregado a lista enlazada de colisiones" << endl;
		}
		// imprime información
		if(tmp==NULL){
			cout << "\n\t >> No se ha encontrado" << endl;		
		}
		else{
			cout << "\n\t >> Se ha encontrado" << endl;
		}
		// imprime
		lista-> imprimir();
	}
}

//función que entrega respuesta sobre si el arreglo está lleno o no
bool Arreglo::is_full(int n, int *arreglo){
	// utiliza un contador para determinar si está lleno o no
	int contador = 0;
	bool llena;
	for(int i=0; i<n; i++){
		if(arreglo[i]!=NULL){
			contador++;
		}
	}
	// compara contador con el largo
	if(contador==n){
		llena = true;
	}
	else{ 
		llena = false;
	}
	return llena;
}
